package main

import (
	"gitee.com/ascend/mapper-go-sdk/example/virtualDevice/driver"
	"gitee.com/ascend/mapper-go-sdk/pkg/service"
)

// main Virtual device program entry
func main() {
	vd := &driver.VirtualDevice{}
	service.Bootstrap("RandomNumber", vd)
}
